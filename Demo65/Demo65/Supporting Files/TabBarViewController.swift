//
//  TabBarViewController.swift
//  Demo65
//
//  Created by sanTEs on 30/04/22.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureControllers()
        
    }
    
    fileprivate func configureControllers(){
        // Home
        let home = UINavigationController(rootViewController: HomeViewController())
        home.tabBarItem.title = "Home"
        home.tabBarItem.image = UIImage(systemName: "house")
        home.tabBarItem.selectedImage = UIImage(systemName: "house.fill")
        
        // Account
        let account = UINavigationController(rootViewController: AccountViewController())
        account.tabBarItem.title = "Account"
        account.tabBarItem.image = UIImage(systemName: "person")
        account.tabBarItem.selectedImage = UIImage(systemName: "person.fill")
        
        
        // Notification
        let notification = UINavigationController(rootViewController: NotificationViewController())
        notification.tabBarItem.title = "Notification"
        notification.tabBarItem.image = UIImage(systemName: "bell")
        notification.tabBarItem.selectedImage = UIImage(systemName: "bell.fill")
        
        self.viewControllers = [home, account, notification]
        self.selectedIndex = self.viewControllers?.firstIndex(of: home) ?? 0
    }
}
