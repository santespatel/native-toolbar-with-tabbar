//
//  DetailsViewController.swift
//  Demo65
//
//  Created by sanTEs on 30/04/22.
//

import UIKit

class DetailsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    private let tabeleDataSource = HomeDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = editButtonItem
        self.tableView.dataSource = self.tabeleDataSource
        self.toolbarItems = [UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: nil)]
    }
}

// MARK: - Configure Edit
extension DetailsViewController {
    override func setEditing(_ editing: Bool, animated: Bool) {
        self.navigationItem.hidesBackButton = editing
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: true)
        self.navigationController?.setToolbarHidden(!editing, animated: true)
        
    }
}
