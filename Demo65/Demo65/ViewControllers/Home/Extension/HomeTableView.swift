//
//  HomeTableView.swift
//  Demo65
//
//  Created by sanTEs on 30/04/22.
//

import UIKit

class HomeDataSource: NSObject, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        21
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        var contentConfiguration = cell.defaultContentConfiguration()
        contentConfiguration.text = "my row number is : \(indexPath.row)"
        contentConfiguration.secondaryText = "my Section is : \(indexPath.section)"
        contentConfiguration.secondaryTextProperties.color = .secondaryLabel
        cell.contentConfiguration = contentConfiguration
        cell.accessoryType = .detailDisclosureButton
        return cell
    }
}


