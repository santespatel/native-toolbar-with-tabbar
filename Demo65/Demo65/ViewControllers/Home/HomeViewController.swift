//
//  HomeViewController.swift
//  Demo65
//
//  Created by sanTEs on 30/04/22.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    private let tabeleDataSource = HomeDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = editButtonItem
        self.tableView.dataSource = self.tabeleDataSource
        
//        self.toolbarItems = [UIBarButtonItem(barButtonSystemItem: .add, target: self, action: nil)]
        
        // ToolBarItem
        self.tabBarController?.setToolbarItems([
            UIBarButtonItem(barButtonSystemItem: .add, target: self, action: nil),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: nil),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: nil)
        ], animated: true)
        
        self.tabBarController?.toolbarItems = [
            UIBarButtonItem(barButtonSystemItem: .add, target: self, action: nil),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: nil),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: nil)
        ]
    }
}

// MARK: - Configure Edit
extension HomeViewController {
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: true)
        
        
        if editing {
            
            if self.tabBarController?.tabBar.isHidden == !editing {
                self.tabBarController?.tabBar.isHidden = editing
                self.tabBarController?.moreNavigationController.setToolbarHidden(false, animated: true)
//                self.tabBarController?.navigationController?.setToolbarHidden(!editing, animated: false)
            }
        } else {
            self.tabBarController?.moreNavigationController.setToolbarHidden(true, animated: true)
//            self.tabBarController?.navigationController?.setToolbarHidden(!editing, animated: false)
            self.tabBarController?.tabBar.isHidden = editing
        }
        
//        self.tabBarController?.tabBar.isHidden = editing
//        self.navigationController?.setToolbarHidden(!editing, animated: true)
    }
}

// MARK: - UITableView Delegate
extension HomeViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let controller = DetailsViewController()
        controller.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
}


// MARK: - Multiple selection methods.
extension HomeViewController {
    /// - Tag: table-view-should-begin-multi-select
    func tableView(_ tableView: UITableView, shouldBeginMultipleSelectionInteractionAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    /// - Tag: table-view-did-end-multi-select
    func tableViewDidEndMultipleSelectionInteraction(_ tableView: UITableView) {
        print("selected item \(tableView.indexPathsForSelectedRows?.compactMap({ $0.row }) ?? [Int]()) ")
    }
}
